/*
    File: fn_lockTimeGlobal.sqf
    Author: Dardo

    Description:
    
*/

private _class = _this select 0;
private _sleepAmount = (missionConfigFile >> _mC >> "LockTimeGlobal") call BIS_fnc_getCfgData;

sleep _sleepAmount;

"enableDumpster" call _class;