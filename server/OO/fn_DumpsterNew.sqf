/*
    File: fn_DumpsterNew.sqf
    Author: Dardo

    Description:
    Create a new dumpster class.
*/
params[["_pos",[],[[]]],["_obj",ObjNull]];
if (isNull _obj || _obj in blackListedDumpsters) exitWith {};
private _isInArray = false;
private _dumpster = ["new", _obj] call OO_Dumpster; //Create a dumpster class
if (isNil "dumpsters") then {
    dumpsters = []
};
//Let's check if the dumpster is already in the array
{
    if (dumpsters isEqualTo[]) exitWith {};
    private _object = "getObj" call _x;
    if (_object isEqualTo _obj) then {
        _isInArray = true;
    };
}
count dumpsters;


if not(_isInArray) then {
    dumpsters pushback _dumpster; //Pushback into a global array our dumpster class
    publicVariable "dumpsters"; //Update the array
};