/*
    File: fn_lockTimeUser.sqf
    Author: Dardo

    Description:
    
*/

private _class = _this select 0;
private _uid = _this select 1;
private _sleepAmount = (missionConfigFile >> _mC >> "LockTimeUser") call BIS_fnc_getCfgData;

sleep _sleepAmount;

["delFromBlackList",_uid] call _class;