#include "header\oop.h"


CLASS("OO_Dumpster")
		PRIVATE VARIABLE("array","position");
		PRIVATE VARIABLE("scalar","trash");
		PRIVATE VARIABLE("object","dumpster");
		PRIVATE VARIABLE("bool","isDisabled");
		PRIVATE VARIABLE("array","blackList");
		PRIVATE VARIABLE("scalar","lastUse");
		
		PUBLIC FUNCTION("object","constructor") {
		    MEMBER("dumpster",_this);
		    MEMBER("trash",(missionConfigFile >> _mC >> "InitialTrash") call BIS_fnc_getCfgData);
		    MEMBER( "position",getPos(MEMBER("dumpster",nil)) );
		    MEMBER("isDisabled",false);
		    MEMBER("blackList",[]);
		    MEMBER("lastUse",0);
		};
				
		PUBLIC FUNCTION("","deconstructor") {
		    DELETE_VARIABLE("position");
		    DELETE_VARIABLE("trash");
		    DELETE_VARIABLE("dumpster");
		};
		
		PUBLIC FUNCTION("","getPos") FUNC_GETVAR("position");
		PUBLIC FUNCTION("","isDisabled") FUNC_GETVAR("isDisabled");
		PUBLIC FUNCTION("","getObj") FUNC_GETVAR("dumpster");
		PUBLIC FUNCTION("","getTrash") FUNC_GETVAR("trash");
		PUBLIC FUNCTION("","getLastUse") FUNC_GETVAR("lastUse");
		
		PUBLIC FUNCTION("","disableDumpster") {
		MEMBER("isDisabled",true);
		};
		PUBLIC FUNCTION("","enableDumpster") {
		MEMBER("isDisabled",false);
		};
		PUBLIC FUNCTION("string","addBlackList") {
		private _bList = MEMBER("blackList",nil);
		_bList pushBack _this;
		MEMBER("blackList",_bList);
		};
		PUBLIC FUNCTION("string","delFromBlackList") {
		private _bList = MEMBER("blackList",nil);
		_bList = _bList - [_this];
		MEMBER("blackList",_bList);
		};
		PUBLIC FUNCTION("string","isInBlackList") {
		private _bList = MEMBER("blackList",nil);
		if (_this in _bList) exitWith { true };
		false;
		};
		PUBLIC FUNCTION("scalar","setTrash") {
			MEMBER("trash",_this);
		};
		PUBLIC FUNCTION("scalar","addTrash") {
			private ["_trash","_toAdd"];
			_trash= MEMBER("trash",nil);
			_toAdd=_this;
			_trash = _trash + _toAdd;
			MEMBER("trash",_trash);
		};
		PUBLIC FUNCTION("scalar","removeTrash") {
			private ["_trash","_toRemove"];
			_trash= MEMBER("trash",nil);
			_toRemove=_this;
			_trash = _trash - _toRemove;
			MEMBER("trash",_trash);
			MEMBER("lastUse",time);
		};
		PUBLIC FUNCTION("object","isInFov") {
			private _unit = _this;
			private _dumpster = MEMBER("dumpster","nil");
			_eyeDV = eyeDirection _unit;
            _eyeD = ((_eyeDV select 0) atan2 (_eyeDV select 1));
            if (_eyeD < 0) then {_eyeD = 360 + _eyeD};
            _dirTo = [_dumpster, _unit] call BIS_fnc_dirTo;
            _eyePb = eyePos _dumpster;
            _eyePa = eyePos _unit;
            if not ((abs(_dirTo - _eyeD) >= 90 && (abs(_dirTo - _eyeD) <= 270)) || (lineIntersects [_eyePb, _eyePa]) ||(terrainIntersectASL [_eyePb, _eyePa])) then {
            true
            } else {
            	false
            }
		};
		
		
ENDCLASS;