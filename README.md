Dumpster Module
===================


This module provides a new job for entertain your players, that is the GarbageMan.
Even if this module is not 100% plug&play, you'll find a bunch of well commented functions and a config file , for integrating the module inside your mission, and adapting it to your liking.

It is very likely that you will encounter issues, as the module is very young itself.
Please point out any problems you'll notice.

----------


Features
-------------



#### <i class=" icon-trash"></i> Object Oriented Dumpsters

 Each time a player discovers a 'new' dumpster, a script creates a class and puts it in a global array,which will be accesible from all players.
 Instead,when a player tries to gather the trash from an 'already stored' dumpster, the previously created class will get used.

> **Note:** Refer to Client\OO\fn_dumpAction.sqf and Server\OO\fn_dumpsterNew.sqf

----------


 
#### <i class="icon-folder-open"></i> Addon Config

The module also provide a rude addon,provided with only a config.cpp, which adds a new item (RPF_Items_Plastic), you can adapt it with your models or textures. 

#### <i class="icon-doc-text-inv"></i> Config.hpp
There is also a basic config which allows you to interact with some main optional features of the module.

> **Extract of the Config:** 
> initialTrash = 64; //Initial trash which a new dumpster should have

> TrashToGather = 16; //Amount of trash to remove, 0 for fully emptying the dumpster

> DismissContainer = false; //Locks the dumpster for ALL users when its trash == 0

> PermaLockUser = false; //Locks the dumpster for the users who picked trash from it,LockTimeUser will be ignored if this is true

> LockTimeGlobal = 0; //Amount in seconds before the dumpster can be used again by all users, this disables LockTimeUser

> LockTimeUser = 0; //Amount in seconds before the dumpster can be used again by a user

> RefillTime = 0; //Amount in seconds before the dumpster gets fully refilled


#### <i class="icon-globe"></i> Dumpsters Refill
The  default refill depends on the number of groundWeaponHolders (items dropped on the ground) which are present.
The more groundWeaponHolders there will be,the more trash will get refilled.
You can edit the config for enabling an easier automatic refill each x minutes.

#### <i class="icon-globe"></i> Stringtable
Just add this entry to the main mission stringtable

    <Key ID="STR_RPF_MODULES_GATHERTRASH">
    	<Original>Pick Trash</Original>
    </Key>
There's also a stringtable in the provided addon.
