/*
    File: fn_initDumper.sqf
    Author: Dardo

    Description:
    Load the header into memory and adds an entry to the InteractionMenu array
*/


_menuItems = [
	[
		["!(isNull (call ClientModules_fnc_fetchDumpster))", "(player distance (call ClientModules_fnc_fetchDumpster)) <= 20", "vehicle player != player"],
		[(localize "STR_RPF_MODULES_GATHERTRASH"), "[] spawn ClientModules_fnc_DumpAction"]
	]
];
{
	RPF_InteractionMenuItems pushBack _x;
}forEach _menuItems;
