/*
    File: fn_DumpAction.sqf
    Author: Dardo

    Description:
    Gather the trash from the nearest dumpster.
    This could create a new class (nearest dumpster)
*/
scopeName "Main";
private _pos = getPos player;
private _obj = [_pos] call ClientModules_fnc_fetchDumpster; //Fetch a new dumpster (Arma Object)
[getpos player,_obj] remoteExecCall["ClientModules_fnc_DumpsterNew", 2]; //Create a class if it's not already defined
private _class = call ClientModules_fnc_selectDumpster;
if (isNil "_class") exitWith {}; //No dumpster
private _isBlackListed = ["isInBlackList",getPlayerUID player] call _class;
private _isDisabled = "isDisabled" call _class;
private _removed;
if (_isBlackListed || _isDisabled) exitWith {
    titleText["Dumpster is Empty", "PLAIN"];
}; //The user already picked up trash from this dumpster

//Prog bar
with uiNamespace do { 
    trashBar = findDisplay 46 ctrlCreate ["RscProgress", -1]; 
    trashBar ctrlSetPosition [ 0, 0.3 ]; 
    trashBar progressSetPosition 0; 
    trashBar ctrlCommit 0; 
}; 
_counter = 21; 
for "_i" from 1 to _counter do { 
    (uiNamespace getVariable "trashBar") progressSetPosition (_i/_counter);
    private _percent= (_i*100)/(_counter-1);
    (uiNamespace getVariable "trashBar") ctrlSetText format["%1 %",_percent];
    if (vehicle player isEqualTo player) exitWith 
    {
        titleText["You left your vehicle,gather interrupted", "PLAIN"];
        breakOut "Main";
    };
    //Stop conditions
    if (!alive player || player getVariable "unconscious") exitWith { breakOut "Main" };
    sleep 1; 
}; 
ctrlDelete (uiNamespace getVariable "trashBar");
titleText["Trash Gathered", "PLAIN"];



//Remove the trash
if ((missionConfigFile >> _mC >> "TrashToGather") > 0) then {
["removeTrash", (missionConfigFile >> _mC >> "TrashToGather") call BIS_fnc_getCfgData] call _class; //Remove 16 unit of trash from the container
} else
{
_removed = "getTrash" call _class; //Amount of trash removed
["removeTrash", "getTrash" call _class] call _class; //Empties the container
};

for [{_i=0}, {_i < _removed}, {_i = _i + ((missionConfigFile >> _mC >> "TrashToGather") call BIS_fnc_getCfgData)}] do {
vehicle player addWeaponCargoGlobal "RPF_Trash_Plastic"; //TODO: Add more kinds of trash -- Medikit testing purposes
};

//Dismiss Check
if ((missionConfigFile >> _mC >> "DismissContainer") call BIS_fnc_getCfgData) then {
    if ("getTrash" call _class isEqualTo 0) then {
        //Cleanup the class
        private _obj = "getObj" call _class;
        [_class] spawn {
            private _class = _this select 0;
            sleep 2;
            private _idx = dumpsters find _class;
            dumpsters deleteAt _idx;
            publicVariable "dumpsters";
        };
        "delete" call _class;
        blackListedDumpsters pushBack _obj;
        publicVariable "blackListedDumpsters";
        breakOut "Main";
    };
};
//Global Timed Lock 
if ((missionConfigFile >> _mC >> "LockTimeGlobal") call BIS_fnc_getCfgData) then {
    "disableDumpster"
    call _class;
    [_class] remoteExec["ServerModules_fnc_lockTimeGlobal", 2];
    breakOut "Main";

} else {
    if ((missionConfigFile >> _mC >> "LockTimeUser") call BIS_fnc_getCfgData != 0) then {
        [_class, getPlayerUID player] remoteExec ["ServerModules_fnc_lockTimeUser", 2];
        breakOut "Main";
    };

};


//User Check
if ((missionConfigFile >> _mC >> "PermaLockUser") call BIS_fnc_getCfgData) then {
    ["addBlackList", getPlayerUID player] call _class;
};