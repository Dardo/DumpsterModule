/*
    File: fn_selectDumpster.sqf
    Author: Dardo

    Description:
    Returns nearest existing dumpster class.
*/
private "_dumpster";
if (isNil "dumpsters" || dumpsters isEqualTo[]) exitWith {
    nil
};

{
    _bool = false;
    _posobj = "getPos"
    call _x; //Call a method which returns the position of the dumpster
    _posplayer = getPos player;

    if (_posobj distance _posplayer < 15) then {
        _bool = ["isInFov", player] call _x; //Call a method in the current class (_x is the class), is the dumpster in player's FOV ?
    };
    if (_bool) exitWith {
        _dumpster = _x
    }; //We got it! Exit loop and return the (nearest) dumpster         
}
forEach dumpsters;
if (isNil "_dumpster") then {
    nil;
} else {
    _dumpster;
};