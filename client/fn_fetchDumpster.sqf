/*
    File: fn_fetchDumpster.sqf
    Author: Dardo

    Description:
    Fetch a new dumpster and return it as a value.
    This function is used for creating a dumpster class.
*/

params [ ["_pos", [0,0,0], [[]] ],["_radius",20, [0] ]];
private _toDelete = [];
private _dumpNear = nearestTerrainObjects [_pos ,["HIDE"],_radius];
if not(_dumpNear isEqualTo []) then {
{
    if (!((str _x find "GarbageContainer") == -1)) then { _toDelete pushback _forEachIndex };

} forEach _dumpNear;

_toDelete sort false;

{ 
  _dumpNear deleteAt _x;  
  false;
} count _toDelete;
};

_dumpNear = _dumpNear + (nearestObjects [_pos,["Land_GarbageContainer_closed_F","Land_GarbageContainer_open_F"],_radius]);
if (_dumpNear isEqualTo [] ) exitWith { objnull };

_dumpNear = _dumpNear select 0; //We need only a single obj
if (_dumpNear in blackListedDumpsters) exitWith { objnull }; //Blacklisted dumpster

_dumpNear;