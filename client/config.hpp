class RPF_dumpsterModule {
InitialTrash = 64; //Initial trash which a new dumpster should have
TrashToGather = 16; //Amount of trash to remove, 0 for fully emptying the dumpster
DismissContainer = false; //Locks the dumpster for ALL users when its trash == 0
PermaLockUser = false; //Locks the dumpster for the users who picked trash from it,LockTimeUser will be ignored if this is true
LockTimeGlobal = 0; //Amount in seconds before the dumpster can be used again by all users, this disables LockTimeUser
LockTimeUser = 0; //Amount in seconds before the dumpster can be used again by a user
RefillTime = 0; //Amount in minutes before the dumpster gets fully refilled
TrashToItem = 2; //Amount of trash for having a trash item (eg:If equals to 4, then the player will have 1 trash item each 4 RPF_Items_Plastic he gathers)
}