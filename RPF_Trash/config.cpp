class CfgPatches
{
	class RPF_Trash
	{
		units[] = {"RPF_Items_Plastic"};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"RPF_General"};
	};
};

class CfgWeapons 
{	class ToolKit;
	class RPF_Items_Base: ToolKit
	{
		count = 1;
		class ItemInfo
		{
			allowedSlots[] = {701,801,901};
			mass = 6;
			scope = 0;
			type = 620;
			uniformModel = "\A3\Weapons_F\Items\Toolkit";
		};
		model = "\A3\weapons_F\ammo\mag_univ.p3d";
	};
	class RPF_Items_Plastic: RPF_Items_Base  //Base class for trash
	{
		displayName = "$STR_RPF_TRASH_PLASTIC_NAME";
		class ItemInfo
		{
			allowedSlots[] = {701,801,901};
			mass = 6;
			scope = 0;
			type = 620;
			uniformModel = "\A3\Weapons_F\Items\Toolkit";
		};
		model = "";
		picture = "\RPF_Trash\Data\RPF_Trash_Generic.paa";
	};
};